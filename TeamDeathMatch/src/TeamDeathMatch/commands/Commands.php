<?php
namespace TeamDeathMatch\commands;

use pocketmine\plugin\PluginBase;
use pocketmine\command\CommandSender;
use pocketmine\command\Command;

use pocketmine\utils\TextFormat as TF;

use TeamDeathMatch\Main;

class Commands{
	
	private $plugin;
	
	public function __construct(Main $plugin){
		$this->plugin = $plugin;
		$this->settings = $this->plugin->settings;
		$this->map = $this->plugin->map;
		$this->areans = $this->plugin->areans;
	}

	public function onCommand(CommandSender $sender, Command $cmd, $label, array $args){
		if(strtolower($cmd->getName()) === "tdm"){
			if(isset($args[0])){
				if($args[0] === "set"){
					if(isset($args[1])){
						$sender->sendMessage(TF::GREEN."Please tap a sign to set a TeamDeathMatch areana for ".$args[1]."!");
						$this->map[$sender->getName()] = $args[1];
					}
				}
				if($args[0] === "reload"){
					$this->settings->reload();
					$this->areans->reload();
					$sender->sendMessage(TF::GREEN."Configs reloaded!");
				}
				if($args[0] === "help"){
					$sender->sendMessage(TF::RED."--------".TF::DARK_PURPLE."TeamDeathMatch".TF::RED."--------\n".TF::GOLD."TeamDeathMacth plugin by SavionLegendZzz".TF::AQUA."(Twitter @SavionLegendZzz)");
					$sender->sendMessage(TF::GREEN."/tdm set [arena name]");
					$sender->sendMessage(TF::GREEN."/tdm reload");
					$sender->sendMessage(TF::RED."------------------------------");
				}
			}else{
					$sender->sendMessage(TF::RED."--------".TF::DARK_PURPLE."TeamDeathMatch".TF::RED."--------\n".TF::GOLD."TeamDeathMacth plugin by SavionLegendZzz".TF::AQUA."(Twitter @SavionLegendZzz)");
					$sender->sendMessage(TF::GREEN."/tdm set [arena name]");
					$sender->sendMessage(TF::GREEN."/tdm reload");
					$sender->sendMessage(TF::RED."------------------------------");
			}
			
		}
	}
}