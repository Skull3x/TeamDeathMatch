<?php 

namespace TeamDeathMatch;

use pocketmine\Server;
use pocketmine\Player;

use pocketmine\event\Listener;

use pocketmine\plugin\PluginBase;

use pocketmine\utils\Config;
use pocketmine\utils\TextFormat as TF;

use TeamDeathMatch\events\EventsManager;
use TeamDeathMatch\tasks\GameTask;
use TeamDeathMatch\commands\Commands;

use pocketmine\command\CommandSender;
use pocketmine\command\Command;


class Main extends PluginBase implements Listener{

public $setter = array();
public $map = array();
public $maps = array();
public $score = array();

public $redPlayers = array();
public $bluePlayers = array();

public function onEnable(){
	@mkdir($this->getDataFolder());
	$this->settings = new Config($this->getDataFolder()."settings.yml", Config::YAML, array("score-to-win" => 30,"game-time" => 900,"message-type" => "popup","prefix" => "[TDM]","helmet" => array(1,1,1),"chestplate" => array(1,1,1),"leggings" => array(1,1,1),"boots" => array(1,1,1),"sign-ready" => array("-----","TDM","Ready","-----"),"sign-queuing" => array("-----","TDM","Queueing","-----"),"sign-running" => array("-----","TDM","Running","-----")));
	$this->areans = new Config($this->getDataFolder()."areans.yml", Config::YAML, array("Maps" => array("Map-1")));
	foreach($this->areans->get("Maps") as $m){
	$this->maps[$m] = $this->settings->get("game-time");	
	}
	echo var_dump($this->maps);
	$this->getServer()->getLogger()->info("[TeamDeathMatch]Loaded");
	$this->getServer()->getPluginManager()->registerEvents(new EventsManager($this),$this);
	$this->getServer()->getScheduler()->scheduleRepeatingTask(new GameTask($this),20); 
	$this->commands = new Commands($this);
}

public function onDisable(){
}

public function giveItems(Player $player){
	$inv = $player->getInventory();
	
	$inv->setContents([]);
	/* by item id */
	$h = $this->settings->get("helmet");
	$c = $this->settings->get("chestplate");
	$l = $this->settings->get("leggings");
	$b = $this->settings->get("boots");
	
	$inv->setHelmet($h[0],$h[1],$h[2]);
	$inv->setChestplate($c[0],$c[1],$c[2]);
	$inv->setLeggings($l[0],$l[1],$l[2]);
	$inv->setBoots($b[0],$b[1],$b[2]);
}

public function sendMessageType(Player $player,$message){
	if($this->settings->get("message-type") === "popup"){
		$p->sendPopup($message);
	}else{
		$p->sendTip($message);
	}
	
}


public function resetSign($map,$int){
	$signconfig = $this->settings->get("sign[{$map}]");
	$this->sign = new Vector3($signconfig["x"],$signconfig["y"],$signconfig["z"]);
	if($this->sign instanceof \pocketmine\tile\Sign){
		if($int === "ready"){
		$signready = $this->settings->get("sign-ready");
		$sign->setText($signready[0],$signready[1],$signready[2],$signready[3]);
		}
		if($int === "queue"){
		$signqueue = $this->settings->get("sign-queuing");
		$sign->setText($signqueue[0],$signqueue[1],$signqueue[2],$signqueue[3]);
		}
		if($int === "running"){
		$signrunning = $this->settings->get("sign-running");
		$sign->setText($signrunning[0],$signrunning[1],$signrunning[2],$signrunning[3]);
		}
	}
}

public function pickTeam($map,Player $player){
	if(count($this->bluePlayers[$map]) and count($this->redPlayers[$map]) === 10){
		return false;
	}
	if(count($this->bluePlayers[$map]) < count($this->redPlayers[$map])){
		$this->bluePlayers[$map][$player->getName()] = array("Player" => $player->getName());
	}else{
		$this->redPlayers[$map][$player->getName()] = array("Player" => $player->getName());
	}
}

public function stopMatch($map){
	$this->maps[$map] = $this->settings->get("playTime");
	$this->resetSign($map,"ready");
	foreach($this->getServer()->getLevelByName($map)->getPlayers() as $player){
		$player->teleport($this->getServer()->getDefaultLevel()->getSafeSpawn());
	}
}

public function checkScore($map){
	if(!in_array($map, $this->score)){
		return;
	}
	if($this->score[$map]["BlueTeam"] === $this->settings->get("score-to-win")){
		
	}
	if($this->score[$map]["RedTeam"] === $this->settings->get("score-to-win")){
		
	}
}

public function nextPoint($map,Player $player){
	if(isset($this->bluePlayers[$map][$player->getName()])){
		$this->score[$map]["BlueTeam"]++;
	}
	if(isset($this->redPlayers[$map][$player->getName()])){
		$this->score[$map]["RedTeam"]++;
	}
}

public function onCommand(CommandSender $sender, Command $command, $label, array $args){
		$this->commands->onCommand($sender, $command, $label, $args);
	}

public function runMatches(){
	foreach($this->areans->get("Maps") as $m){
		$level = $this->getServer()->getLevelByName($m);
		$this->maps[$m]--;
		if($level !== null){
			foreach($level->getPlayers() as $p){
				if($this->maps[$m] === 0){
					$this->stopMatch($m);
				}
			$this->sendMessageType($p,$this->maps[$m]);
				
			}
		}
	}
}

}